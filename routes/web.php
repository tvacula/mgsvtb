<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Post-wedding stuff
Route::get('/', function () {
    return view('post_wedding');
})->name('home');
Route::get('/wedding', function () {
    return view('pw_photos');
})->name('pw_photos');
Route::get('/contacts', function () {
    return view('pw_contacts');
})->name('pw_contacts');
Route::get('/photos_slavicek', function () {
    return view('photos_slavicek');
})->name('photos_slavicek');

// Title page
Route::get('/introduction', function () {
    return view('welcome');
})->name('introduction');

// Mira and Gaba
Route::get('/history', function () {
    return view('history');
})->name('history');
Route::get('/engagement', function () {
    return view('engagement');
})->name('engagement');
Route::get('/photoshoot', function () {
    return view('photoshoot');
})->name('photoshoot');

// Programme
Route::get('/before', function () {
    return view('before');
})->name('before');
Route::get('/ceremony', function () {
    return view('ceremony');
})->name('ceremony');
Route::get('/party', function () {
    return view('party');
})->name('party');

// Acomodation
Route::get('/acommodation', function () {
    return view('acommodation');
})->name('acommodation');

// Maps/Contacts
Route::get('/whoiswho', function () {
    $people = array(
        "Gábinka" => array("nevěsta", "604 421 214", "gabahruskova@gmail.com"),
        "Míra" => array("ženich", "723 701 402", "mirekjr.barton@gmail.com"),
        "Lucinka " => array("svědkyně", "736 171 053", "luciehruskova99@gmail.com"),
        "ToSu " => array("svědek", "721 774 222", "tosucz@gmail.com"),
        "Máca" => array("družička", "732 159 861", "macazubrova@seznam.cz"),
        "Monča" => array("družička", "775 941 125", "Moncak7@seznam.cz"),
        "Anežka" => array("družička", "721 310 601", "anezka.bartonova@email.cz"),
    );

    return view('whoiswho', compact('people'));
})->name('whoiswho');

Route::get('/maps', function () {
    return view('maps');
})->name('maps');
// Gifts, FAQ, RSVP
Route::get('/faq', function () {
    return view('faq');
})->name('faq');

Route::get('/gifts', function () {
    return view('gifts');
})->name('gifts');

/*Route::post('/sendanemail', function (\Illuminate\Http\Request $request) {

    $data = array(
        'mail' => $request->mailfield,
        'dotaz' => $request->dotazfield
    );

    Mail::send(['text' => 'Dotaz od:'.$request->mailfield."<br>Dotaz:".$request->dotazfield], $data, function($message) {
        $message->from('dotaz@mgsvtb.eu');
        $message->to('tosucz@gmail.com')->subject('kurva jebat');
    });
})->name('sendmail');*/
