@extends('layout')

@section('title')
    @lang('web.mainMenu6')
@endsection

@section('content')
    <h2>Seznam darů</h2>
    <p>Samozřejmě nejlepším darem, jakým nám můžete dát, je již pouhá Vaše přítomnost při našem velkém dni. Avšak, jak již bývá zvykem, alespoň někteří z Vás nás zajisté budou chtít obdarovat něčím hmatatelnějším. Abychom předešli tomu, že se nám sejde sedm porcelánových servisů, deset toustovačů a tři lustry, dovolujeme si Vám předložit tzv. wishlist, tedy seznam našich přání, a to <a target="_blank" rel="noopener noreferrer" href="https://docs.google.com/spreadsheets/d/1-3UafALHgDbMXOMjhy_YJmZVdtg1ivusnyxVAVYIrNw/edit?usp=sharing">ZDE</a>. Pokud se Vám nějaká položka z něj bude líbit, napište, prosíme, vedle něj své jméno, aby se nám takových darů nesešlo více. Rozhodně však nečekáme, že se nám sejde vše! Seznam je širší, aby bylo z čeho vybírat, neciťte se, prosím, špatně jen kvůli tomu, že mnoho z darů zůstává nezabraných.</p>
    <p>Pokud se Vám žádný z nich nelíbí a stejně byste nás chtěli nějak obdarovat, máte dvě možnosti - buď svůj originální a skvělý nápad na dar, který nás nenapadl, předložíte našim svědkům (kteří Vám řeknou, jak přesně dobrý ten nápad ve skutečnosti je), nebo nám můžete darovat nějaký finanční obnos známou formou obálky. Takové dary jsou velmi vítány, protože domácnost máme sice v podstatě zařízenou, avšak jak každý ví, život študákův je životem nákladným.</p>
@endsection