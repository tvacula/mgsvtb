@extends('layout')

@section('title')
    @lang('web.mainMenu3_sub1')
@endsection

@section('content')
    <h2>Před obřadem</h2>
    <p>Zde najdete informace o tom, jak a kdy nás poctít svou vznešenou přítomností.</p>
    <p>Do Retroparku Sejfy, kde se bude jak nacházet ubytování, tak hodovat po obřadu, lze dorazit již od (ne zas tak) brzkého rána, řekněme tak od osmi hodin. Bohužel nejsme schopni zajistit to, že se takto brzy budete rovnou moci ubytovat, ale bude k dispozici místnost(i), ve které bude možno se přestrojiti z cestovního oblečení do slavnostních šatů.</p>
    <p>Příslušníci rodných klanů, kteří s námi zasednou k tabuli, nechť si jsou vědomi toho, že po hodině jedenácté začneme hodovati, ať už nás svou přítomností poctí, či ne.</p>
    <p>Pro ostatní, co nejsou součástí těchto klanů, bude také připraveno malé pohoštění.</p>
    <p>Zároveň prosíme řidiče motorových vozů, aby se zdrželi ještě protentokrát omamných nápojů, neboť po ukončení hostiny bude zanedlouho následovat přesun do Horního Maršova, kde se bude konat samotný obřad.</p>
    <p>Pro ty, co tomuto lákadlu nemohou či nechtějí odolávat, je možnost přepravy velkým, dvacetimístným vozem na obřad i zpět. Zájemce o tuto možnost prosíme, aby se nám předem nahlásili. Kdo se pozdě přihlásí, má při velkém zájmu smůlu.</p>

    <hr>
    <p>Program před obřadem:</p>
    <table class="program">
        <tr>
            <td>11:00</td><td class="filler" rowspan="2"></td><td>Rodinný oběd</td>
        </tr>
        <tr>
            <td>13:15</td><td>Odjezd ze Sejfů do kostela</td>
        </tr>
    </table>
@endsection