@extends ('layout')

@section('title')
    @lang('web.pw_photos')
@endsection

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('slick/slick.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('slick/slick-theme.css') }}"/>
    <script type="text/javascript" src="{{ asset('slick/slick.min.js') }}"></script>
@endsection

@section('content')
    <h2>Iluminace ze svatby</h2>

    <p>Zde můžete najít veškeré iluminace tak, jak nám byly předány zručnými umělci, k Vašemu nahlédnutí.</p>

    <p>Iluminace hlavního iluminátora, pana Jana Slavíčka a jeho sličné ženy: <a href="https://photos.app.goo.gl/zr2QZ3zMMMHqKebV7" target="_blank">Odkaz</a></p>

    <p>Iluminace mladého umělce Jeníka: <a href="https://photos.app.goo.gl/oRXff4yatpzmg2ua8" target="_blank">Odkaz</a></p>

    <p>Iluminace mocného civilizovaného barbara Fluxe: <a href="https://photos.app.goo.gl/DwrWRoPRbnGsGoaB7" target="_blank">Odkaz</a></p>

@endsection