@extends('layout')

@section('title')
    @lang('web.mainMenu5_sub2')
@endsection

@section('content')
    <h5 class="text-center">Zde najdete mapu parkoviště a kostela v Horním Maršově:</h5>
    <iframe src="https://api.mapy.cz/frame?params=%7B%22x%22%3A15.819135010242462%2C%22y%22%3A50.654136390420234%2C%22base%22%3A%221%22%2C%22layers%22%3A%5B%5D%2C%22zoom%22%3A16%2C%22url%22%3A%22https%3A%2F%2Fmapy.cz%2Fs%2F2SDu0%22%2C%22mark%22%3Anull%2C%22overview%22%3Afalse%7D&amp;width=500&amp;height=333&amp;lang=cs" width="100%" height="333" style="border:none" frameBorder="0"></iframe>

    <h5 class="text-center padding-top-10">Zde je mapa, jak se ze silnice u Mladých Buků dostat na Retropark Sejfy:</h5>
    <iframe src="https://api.mapy.cz/frame?params=%7B%22x%22%3A15.842835659277313%2C%22y%22%3A50.613241072070245%2C%22base%22%3A%221%22%2C%22layers%22%3A%5B%5D%2C%22zoom%22%3A14%2C%22url%22%3A%22https%3A%2F%2Fmapy.cz%2Fs%2F2SDwg%22%2C%22mark%22%3Anull%2C%22overview%22%3Afalse%7D&amp;width=500&amp;height=333&amp;lang=cs" width="100%" height="333" style="border:none" frameBorder="0"></iframe>
@endsection