@extends ('layout')

@section('title')
    @lang('web.mainMenu1')
@endsection

@section('content')
    <h2>Vítejte na našich svatebních stránkách.</h2>
    <p>Najdete zde vše, co potřebujete vědět o naší svatbě, souvisejících záležitostech a i něco o nás.<p>
    <p>Informace budou průběžně doplňovány, takže se, prosím, neostýchejte a navštěvujte tyto stránky, jak jen srdce touží. Obzvláště důležité je kouknout sem tak týden před svatbou, abyste měli všechny důležité informace.</p>

    <p>Z toho nejdůležitějšího:</p>
    <ul>
        <li>Obřad se bude konat v kostele v Horním Maršově od 14:00. Pokud hodláte dorazit až na obřad, prosíme, nechte si dostatečnou časovou rezervu.</li>
        <li>Kdo z Vás je pozván na slavnostní oběd v rodinném kruhu, doražte, prosíme, nejpozději v 11:00 do Retroparku Sejfy.</li>
        <li>V Retroparku Sejfy se bude odehrávat i veškerá zábava, která následuje po obřadu. Podrobný program najdete <a href="{{ route('party') }}">zde</a>.</li>
        <li>Prosíme také, abyste se pokud možno co nejdříve zapsali do ubytovací tabulky, kterou naleznete <a target="_blank" rel="noopener noreferrer" href="https://docs.google.com/spreadsheets/d/10E4AmyqbLTT2RhPtg859WgNnYiG-r1F84ydMmahXLjM/edit?usp=sharing">zde</a>. Potřebujeme to vědět kvůli tomu, aby mohly být pokoje patřičně připraveny, a to do <strong>30. 8. Kdo do té doby nebude zapsán ani nám nedá vědět, že nepřespává, tomu bude pokoj a spolubydlící přidělen.</strong></li>
        <li><strong>Ubytování je pro svatebčany zdarma, takže se nebojte a vybírejte dle libosti.</strong></li>
        <li>Pro dámy - pokud plánujete brát si na svatbu boty na úzkém podpatku, nedělejte to, ledaže byste byly opravdu zarputilé vyznavačky podpatkové turistiky. Berte prosím na vědomí, že svatba se odehrává v Krkonoších a jak okolí kostela, tak Retropark Sejfy neoplývají ani tak betonem či dlažbou, jako spíše štěrkovými cestami a travnatými plochami.</li>
    </ul>

    <hr>
    <p>Program svatebního dne:</p>
    <table class="program">
        <tr>
            <td>11:00</td><td class="filler" rowspan="9"></td><td>Rodinný oběd</td>
        </tr>
        <tr>
            <td>13:15</td><td>Odjezd ze Sejfů do kostela</td>
        </tr>
        <tr>
            <td>14:00</td><td>Začíná obřad</td>
        </tr>
        <tr>
            <td>16:00 - 16:30</td><td>Příjezd zpět na Sejfy, pečení kýty</td>
        </tr>
        <tr>
            <td>16:30</td><td>Ubytovávání, čekání na novomanžele (kteří se mezitím fotí)</td>
        </tr>
        <tr>
            <td>18:00</td><td>Oficiální zahájení, proslov otce nevěsty, začátek rautu</td>
        </tr>
        <tr>
            <td>18:45</td><td>Proslov, házení kytice, krájení dortu, novomanželské hry</td>
        </tr>
        <tr>
            <td>20:00</td><td>První tanec novomanželů, tanec s rodiči, otevření tanečního parketu a volná</td>
        </tr>
        <tr>
            <td></td><td>Zábava</td>
        </tr>
    </table>
@endsection