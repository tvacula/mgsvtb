@extends('layout')

@section('title')
    @lang('web.mainMenu3_sub2')
@endsection

@section('content')
    <h2>Obřad</h2>
    <p>Obřad samotný se bude konat ve dvě hodiny odpolední, či, jak někteří tento čas zapisují, ve 14:00 středoevropského času, a to v horním kostele v Horním Maršově (viz mapu).</p>
    <p>Je to ten starý, nově opravený kostel ve slohu renesančním, stojící naproti faře na kopečku. Také je to ten s tím pěkným hřbitovem. Své vozy, prosíme, zaparkujte dole u restaurace Na Kopečku (opět viz mapu). Malinké parkoviště přímo před kostelem je vyhrazeno pro vůz nevěsty.</p>
    <p>Samotný obřad bude probíhat po dobrém křesťanském způsobu, ritu římskokatolického, proto prosíme ty, kteří s takovými záležitostmi příliš zkušeností nemají, aby vytrvali a nerušili jeho průběh, i když se jim bude zdát příliš dlouhý. Pokud jste na pochybách, co dělat, následujte zkrátka pokynů kněze a vstávejte, když vstávají ostatní, stejně tak sedejte atd. Zpěv se od Vás, k úlevě mnohých, nečeká.</p>
    <p>Hudební doprovod bude pod taktovkou Matěje Havla a zajištěn Scholou Pouchov.</p>
    <p>Zároveň také prosíme, abyste si uschovali své iluminační a fotografické přístroje pro hodovací část a nechali zvěčnění obřadu na našem dojednaném iluminátorovi. Tak si budete moci obřad plně vychutnat jak vy, tak vaši spolusedící.</p>
    <p>Co se týče oblečení, prosíme Vás o alespoň minimální míru formálnosti. V kostele bude tak jako tak nejspíše chladněji, takže pánové se nemusí bát toho, že se ve svých sakách uškvaří zaživa. Po obřadu bude také následovat společná fotografie, takže zajisté i Vy budete chtít vypadat na ní co nejlépe. Pro příznivce cizích slov - vhodný dress code je semi-formal.</p>

    <hr>
    <p>Program obřadu a událostí kolem:</p>
    <table class="program">
        <tr>
            <td>13:15</td><td class="filler" rowspan="3"><td>Odjezd ze Sejfů do kostela</td>
        </tr>
        <tr>
            <td>14:00</td><td>Začíná obřad</td>
        </tr>
        <tr>
            <td>16:00 - 16:30</td><td>Konec obřadu, příjezd zpět na Sejfy, pečení kýty</td>
        </tr>
    </table>
@endsection