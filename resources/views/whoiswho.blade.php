@extends('layout')

@section('title')
    @lang('web.mainMenu5_sub1')
@endsection

@section('content')
    <p>Protože je dost dobře možné, že jste zde na těchto stránkách nenašli všechny informace, po kterých Vaše srdce toužilo, můžete se doptat kohokoliv z nás na nasledujících číslech a adresách. Prosíme, abyste především těsně před svatbou a hlavně ve svatební den bombardovali dotazy nikoliv nevěstu ani ženicha, nýbrž jednoho z našich svědků. <!-- Zároveň také můžete využít kontaktního formuláře níže.--> </p>
    <div class="row namecards">
    @foreach($people as $key => $value)
        <div class="col-12 col-md-5 namecard">
            <h3 class="field-name text-center">{{ $key }}</h3>
            <p class="field-role text-center">{{ $value[0] }}</p>
            <p class="field-phone text-center">{{ $value[1] }}</p>
            <p class="field-mail text-center">{{ $value[2] }}</p>
        </div>
    @endforeach
    </div>
    <!--<hr>-->
    <div class="row faqform justify-content-center">
        <h3 class="form-heading text-center">Zeptejte se nás</h3>
        <form action="mailto:tosucz@gmail.com" method="post" enctype="text/plain">
            <div class="form-group text-center">
                <label for="inputEmail">Váš email</label>
                <input type="email" class="form-control" id="inputEmail" name="emailfield" aria-describedby="emailHelp" placeholder="email@email.cz">
                <small id="emailHelp" class="form-text text-muted">Email je uchován pouze pro účel odpovědi na dotaz.</small>
            </div>
            <div class="form-group text-center">
                <label for="textareaDotaz">Váš dotaz</label>
                <textarea class="form-control" id="textareaDotaz" name="dotazfield" placeholder="Chtěl/a bych se zeptat..." rows="3"></textarea>
            </div>
            <button type="submit" class="btn btn-primary sendButton">Odeslat</button>
        </form>
    </div>
@endsection