@extends('layout')

@section('title')
    @lang('web.mainMenu4')
@endsection

@section('content')
    <h2>Ubytování</h2>

    <p>Vzhledem k tomu, že svatba i následující oslava se koná v Krkonoších, věříme, že se Vám asi nebude zrovna dvakrát chtít někdy o půlnoci řídit až zpět do Hradce nebo jiných, ještě vzdálenějších končin a nadto se celý večer oddávat jen vodě a džusu. Proto pro Vás máme dobrou zprávu - Retropark Sejfy má dostatečné ubytovací kapacity pro všechny! Stačí si jen vybrat. Prosíme Vás ovšem o pochopení, že při větším zájmu o lepší a lépe vybavené pokoje na hotelu dáme přednost rodině a svědkům, abychom je měli při sobě a aby především naši prarodiče mohli spát v pohodlí dále od hluku bujarých oslav.</p>
    <p class="font-weight-bold">Ubytování je pro svatebčany zdarma, takže se nebojte a vybírejte dle libosti.</p>
    <p>Fotografie a popis pokojů najdete zde pod záložkou Ubytování: <a target="_blank" rel="noopener noreferrer" href="http://www.lesniplovarna.cz/">www.lesniplovarna.cz</a></p>
    <p>Zde najdete tabulku, kde jsou rozepsány jednotlivé ubytovací kapacity: <a target="_blank" rel="noopener noreferrer" href="https://docs.google.com/spreadsheets/d/10E4AmyqbLTT2RhPtg859WgNnYiG-r1F84ydMmahXLjM/edit?usp=sharing">Google tabulka</a></p>
@endsection