@extends ('layout')

@section('title')
    @lang('web.post_wedding')
@endsection

@section('content')
    <h2>Po svatbě</h2>

    <p>Tak, po předlouhém čekání se nám konečně podařilo dát dohromady další část našich stránek. Už jsme Vás napínali dostatečně dlouho - svatební iluminace dorazily! A že jich není málo. Zde se jich můžete nabažit do sytosti:</p>
    <p><a href="{{ route('pw_photos') }}">Svatební iluminace</a></p>

    <p>Teď však k něčemu ještě důležitějšímu. Jak mladý pán, tak mladá paní Bartoňovi tímto upřímně a zhluboka děkují všem, kteří s námi mohli strávit onen velký den, těm, kdo nám s jeho uskutečněním pomohli i všem těm, kdo si na nás vzpomněli třeba i jen pěkným slovem na ozdobném papíře. Z té nádherné radosti a lásky, kterou jste nás zahrnuli, se budeme usmívat ještě dlouho.</p>

    <p>A nakonec, protože jsme zřejmě několik lidí alespoň něčím inspirovali a spousta z Vás měla zvídavé otázky ohledně cukrářky, květin, cateringu a podobně, seznam našich dodavatelů a podobných šikulů najdete zde:</p>
    <p><a href="{{ route('pw_contacts') }}">Dodavatelé</a></p>

    <p>Na závěr už zbývá jen říct, že tyto stránky budou funkční ještě zhruba do dubna či května 2019, kdy už vše, co zde najdete, budete muset žádat od nás osobně. Nezapomeňte si tedy zapsat či stáhnout, co budete potřebovat.</p>

    <p>Bylo to s Vámi úžasné a ještě jednou děkujeme.</p>

    <p><strong><em>Míra a Gábinka Bartoňovi</em></strong></p>
@endsection
