@extends ('layout')

@section('title')
    @lang('web.pw_contacts')
@endsection

@section('content')
    <h2>Dodavatelé, kontakty</h2>

    <p>Zde najdete seznam všech dodavatelů, iluminátorů, fotografů a podobných lidí znalých svého řemesla, kteří se podíleli na svatbě tím, co jsme sami nesvedli.</p>

    <h4>Fotografové</h4>

    <ul>
        <li>Hlavní fotograf - Jan Slavíček a jeho žena</li>
        <li>Další fotografové - Jan Jírovec, Dominik Luksch</li>
        <li>Předsvatební iluminace - Janek Sedlář (<a href="http://www.janeksedlar.com/" target="_blank">http://www.janeksedlar.com/</a>)</li>
    </ul>

    <h4>Jídlo</h4>

    <ul>
        <li>Catering - hlavní část (oběd, slaný raut, čokoládová fontána) - Retropark Sejfy (<a href="http://www.lesniplovarna.cz/" target="_blank">http://www.lesniplovarna.cz/</a>)</li>
        <li>Sladkosti a dort - Sladký tečky (<a href="https://cs-cz.facebook.com/sladkytecky/" target="_blank">https://cs-cz.facebook.com/sladkytecky/</a>)</li>
        <li>Moravské (vlčnovské) koláčky - Pekárna Javor (<a href="http://www.vlcnovske-vdolecky.cz/" target="_blank">http://www.vlcnovske-vdolecky.cz/</a>)</li>
    </ul>

    <h4>Výzdoba a ostatní</h4>

    <ul>
        <li>Květinová výzdoba - Květiny La Vital (<a href="http://www.kvetinyvital.cz/" target="_blank">http://www.kvetinyvital.cz/</a>)</li>
        <li>Svatební šaty - svatební salon Le Monika (<a href="http://salon.lemonika.cz/" target="_blank">http://salon.lemonika.cz/</a>)</li>
        <li>Kapela - Pavouk Čichal Salám (<a href="https://www.facebook.com/Pavoukcichalsalam/" target="_blank">https://www.facebook.com/Pavoukcichalsalam/</a>)</li>
        <li>Ostatní výzdoba - aliexpres.com</li>
        <li>Ubytování, catering, zázemí a vůbec všechno - Retropark Sejfy (<a href="http://www.lesniplovarna.cz/" target="_blank">http://www.lesniplovarna.cz/</a>)</li>
    </ul>
@endsection