<nav class="navbar navbar-expand-md navbar-dark bg-golden">
    <div class="container">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse align-center navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}"></a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownpw" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @lang('web.post_wedding')
                    </a>
                    <div class="dropdown-menu bg-golden" aria-labelledby="dropdownpw">
                        <a class="dropdown-item bg-golden" href="{{ route('pw_photos') }}">@lang('web.pw_photos')</a>
                        <a class="dropdown-item bg-golden" href="{{ route('pw_contacts') }}">@lang('web.pw_contacts')</a>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}">@lang('web.mainMenu1')</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @lang('web.mainMenu2')
                    </a>
                    <div class="dropdown-menu bg-golden" aria-labelledby="dropdown1">
                        <a class="dropdown-item bg-golden" href="{{ route('history') }}">@lang('web.mainMenu2_sub1')</a>
                        <a class="dropdown-item bg-golden" href="{{ route('engagement') }}">@lang('web.mainMenu2_sub2')</a>
                        <a class="dropdown-item bg-golden" href="{{ route('photoshoot') }}">@lang('web.mainMenu2_sub3')</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @lang('web.mainMenu3')
                    </a>
                    <div class="dropdown-menu bg-golden" aria-labelledby="dropdown2">
                        <a class="dropdown-item bg-golden" href="{{ route('before') }}">@lang('web.mainMenu3_sub1')</a>
                        <a class="dropdown-item bg-golden" href="{{ route('ceremony') }}">@lang('web.mainMenu3_sub2')</a>
                        <a class="dropdown-item bg-golden" href="{{ route('party') }}">@lang('web.mainMenu3_sub3')</a>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('acommodation') }}">@lang('web.mainMenu4')</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @lang('web.mainMenu5')
                    </a>
                    <div class="dropdown-menu bg-golden" aria-labelledby="dropdown3">
                        <a class="dropdown-item bg-golden" href="{{ route('whoiswho') }}">@lang('web.mainMenu5_sub1')</a>
                        <a class="dropdown-item bg-golden" href="{{ route('maps') }}">@lang('web.mainMenu5_sub2')</a>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('gifts') }}">@lang('web.mainMenu6')</a>
                </li>
                {{--
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @lang('web.mainMenu5')
                    </a>
                    <div class="dropdown-menu bg-golden" aria-labelledby="dropdown3">
                        <a class="dropdown-item bg-golden" href="{{ route('whoiswho') }}">@lang('web.mainMenu5_sub1')</a>
                        <a class="dropdown-item bg-golden" href="{{ route('maps') }}">@lang('web.mainMenu5_sub2')</a>
                        <a class="dropdown-item bg-golden" href="{{ route('faq') }}">@lang('web.mainMenu5_sub3')</a>
                    </div>
                </li> --}}
            </ul>
        </div>
    </div>
</nav>