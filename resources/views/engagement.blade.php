@extends('layout')

@section('title')
    @lang('web.mainMenu2_sub2')
@endsection

@section('content')
    <h2>@lang('web.mainMenu2_sub2')</h2>
    <div class="row">
    <p>Varování: ten název zde není jenom pro legraci, ta cesta byla opravdu dlouhá. A tak musí být dlouhý i kronikový záznam o ní. Takže pokud se na ni chcete vydat, uvařte si pořádný hrnek kafe nebo čaje a pohodlně se usaďte.</p>
    <p>Když jsem byla malá, vymyslela jsem si - po vzoru všech správných princezen z pohádek - pro svého vyvoleného tři úkoly. O mnoho let později jsem si na ně vzpomněla a překvapeně zjistila, že Míra všechny tři úkoly opravdu splnil, a že bych si ho tedy asi měla vzít. To jsem ale nepočítala s tím, že napřed budu muset pár úkolů splnit i já.</p>
    <p>Měl to být jenom obyčejný výlet. Po dlouhém přesvědčování jsem totiž Míru nakonec uprosila, abychom společně něco podnikli a užili si pěkné květnové počasí. Prvním varováním mělo být, když můj milý prohlásil, že výlet sice naplánoval, ale mám na něj vyrazit sama. Dalším varováním bylo, že jsme spolu dva dny před osudným 7. 5. 2017 skoro nemluvili, protože Míra byl celou dobů bůhví kde. To už jsem začínala tušit, že se něco chystá, nikdy by mě však nenapadlo, co to bude.</p>
    <p>Až večer před plánovaným výletem mi Míra poslal nějaké bližší informace - vlastně veršované zadání prvního úkolu. Měla jsem v pravé poledne vyrazit s batůžkem na naše tajné místo. Zvláštní bylo, že jsem musela vyrazit bez oběda. Ještě zvláštnější bylo, že jsem si s sebou do batůžku měla sbalit společenské boty. Ještě, že jsem je ale neměla na sobě, protože mě čekala cesta neprostupným křovím a blátem. Ale statečně jsem vytrvala - to jsem ještě měla dost sil v zásobě - a mohla jsem Mírovi poslat první fotku se splněným úkolem. Obratem mi přišla odpověď. Další básnička, další místo, kam jsem se měla vydat. Vzdálené přes půl Hradce. V tu dobu už jsem byla po kolena zablácená, popálená od kopřiv a boty mi pěkně čvachtaly. Tak jsem se doma přezula a převlékla a vyrazila dál.</p>
    <p>Míra mě takhle postupně navedl až do naší oblíbené indické restaurace Maharádža, kde na mě už čekal a společně jsme si dali oběd. Čekala jsem, že tím moje samostatné putování skončilo a že teď už něco podnikneme spolu. Pletla jsem se. Míra po obědě zase vesele odběhl neznámo kam a já jsem pokračovat sama. Ještě mi řekl, ať nejezdím autobusem, že bych tam byla moc rychle. I když byl květen, počasí se vydařilo doslova aprílové. Chvíli bylo nesnesitelné horko, chvíli jsem mokla. Musím přiznat, že v tu chvíli jsem měla chuť porušit pravidla a přece jenom si autobusem popojet.</p>
    <p>Ještě jedna věc. Míra nikdy nenapsal přímo, kde mám splnit další úkol. V básničkách, které mi posílal, se vždycky skrývala nápověda, kterou jsem musela odhalit a rozluštit. Někdy to bylo snadné - třeba místo, kde jsme se poprvé políbili. Ale kdo si má pamatovat, kde byla naše první hádka? Nebo který dům Mírovi připomíná dům Becky Tatcherové z Toma Sawyera? I úkoly se jeden od druhého lišily. Někde jsem musela vyfotit něco kulatého, někde zase zvířátko, na jednom místě jsem si měla dát Kofolu. Ale všechny úkoly a místa měly jedno společné - nás dva.</p>
    </div>
    <div class="row">
        <div class="col-4 image">
            <img class="img-fluid" src="{{ asset('images/engsm1.jpg') }}">
        </div>
        <div class="col-4 image">
            <img class="img-fluid" src="{{ asset('images/engsm2.jpg') }}">
        </div>
        <div class="col-4 image">
            <img class="img-fluid" src="{{ asset('images/engsm3.jpg') }}">
        </div>
    </div>
    <div class="row">
        <p>Protože jsem během své epické cesty křížem krážem po Hradci neměla co dělat, přemýšlela jsem, co tohle všechno znamená. A proč s sebou, zatraceně, musím celou dobu tahat ty boty na podpatku? Žádost o ruku mě samozřejmě napadla, ale vzhledem k tomu, že Míra má občas sklony dělat takováhle velkolepá gesta jen tak, nebyla jsem si úplně jistá. A jak se slunce pomalu sklánělo k západu, začínalo mi být všechno jedno, jenom aby už byl konec.</p>
        <p>Poslední místo jsem nemusela uhodnout z žádné hádanky. Míra mi poslal GPS souřadnice a ani to nebylo moc daleko. Jenom uprostřed lesa, nikde kolem žádný záchytný bod. To už jsem byla úplně ztracená. Ale nakonec jsem přece jen došla na určené místo. Žádná Hora Osudu, žádný Kámen Mudrců ani Kamenný stůl tam na mě nečekal. Bylo tam něco lepšího.</p>
        <p>Uprostřed lesa, na loňském jehličí, stál dřevěný stůl, na kterém byla naskládaná doslova královská hostina. Ke stolu byly přistaveny dvě židle, vlastně spíš křesílka, ve kterých jsem poznala panské sesličky družiny z Dobřenic. Ke středověkému nábytku ladil Míra, oblečený pěkně dobově v suknici, nohavicích a s mečem připnutým u pasu.</p>
        <p>Samozřejmě že já jsem do toho kouzelného obrazu ve své větrovce, džínách a botách od bahna moc nezapadala. Ale Míra pamatoval i na to. Čekaly tam na mě krásné zelené šaty (které se mi podařilo obléct obráceně a zjistila jsem to až další den z fotek) a silonky. A konečně jsem pochopila, proč jsem celý den s sebou tahala ty taneční boty.</p>
    </div>
    <div class="row">
        <div class="col-12 image">
            <img class="img-fluid mx-auto d-block" src="{{ asset('images/eng1.jpg') }}">
        </div>
    </div>
    <div class="row">
        <p>Moje podezření, na chvíli uspané namáhavou cestou, se opět začalo probouzet. A vzrostlo ještě víc, když jsem si všimla stativu s kamerou, stojícího kousek opodál. A nemohla jsem na sobě nechat nic znát, abych to náhodou Mírovi nezkazila. Takže jsme oba jedli výbornou večeři a já jsem svému milému pěkně barvitě líčila všechna utrpení, která jsem za ten den musela překonat, abych se k němu dostala.</p>
        <p>A pak to konečně přišlo. Míra vstal, pokynul mi, abych se zvedla taky, poklekl přede mnou, vytáhl malou sametovou krabičku a pronesl ta očekávaná slova:</p>
        <p>“Vezmeš si prosím… tyto náušnice?”</p>
        <p>Co???</p>
        <p>Na chvíli jsem nevěděla, co mám dělat. Nepřeslechla jsem se, v sametové krabičce se skutečně skrývaly zlaté náušnice. A navíc jaké náušnice - ve tvaru žaludů, ozdobené dubovými lístky a s kuličkami uvnitř, které (když se člověk dobře zaposlouchá) lehce cinkají. Přesně takové, jako nosí jedna z mých nejoblíbenějších literárních postav (mimochodem pokud vás baví dlouhé a epické cesty - a proč byste jinak četli tohle - udělejte si radost a přečtěte si pentalogii Belgariad od Davida Eddingse… to je takový soukromý tip). Takže jsem právě v rukou držela ten nejlepší dárek jaký mi kdo kdy mohl dát… a neměla jsem z něj takovou radost, jak bych měla mít, protože jsem tajně doufala v něco jiného.</p>
        <p>Jsem přesvědčená, že Míra v tu chvíli všechno tohle věděl a královsky se při tom bavil.</p>
        <p>Znovu jsme se posadili a zase si začali povídat a na stole zbylo ještě trochu jídla a hodně pití. Mezi stromy se začaly prohlubovat stíny, po svícnech stékal vosk z polovyhořelých svíček a Míra po mě celou tu dobu tajemně pokukoval. Pak si zřejmě řekl, že už toho napínání bylo dost a zase se zvedl ze svého křesílka.</p>
        <p>Pokleknutí. Sametová krabička. A věta, na kterou jsem čekala.</p>
        <p>“Vezmeš si mě?”</p>
    </div>
    <div class="row">
        <div class="col-12 image">
            <img class="img-fluid mx-auto d-block" src="{{ asset('images/eng2.jpg') }}">
        </div>
    </div>
    <div class="row">
        <p>Ráda bych napsala, že jsem odpověděla procítěným a důstojným “ano”. Ale toho už jsem v tu chvíli nebyla schopná. A tak jsem udělala tu druhou nejlepší věc: rozbrečela jsem se, vrhla se Mírovi do náruče a zaštkala něco, co by se dalo při dobré vůli přeložit jako “jooooooo”.</p>
        <p>A tím skončilo moje (a Mírovo) velké putování za prstenem. Všechny nástrahy a úkoly se nám oběma podařilo splnit a zjistili jsme, že s tím, co přijde dál už budeme chtít bojovat spolu. A co vlastně přijde dál? Nohy na stůl a šťastně až do smrti? Nebo další dobrodružství? To zatím nevíme, ale už se nemůžeme dočkat.</p>
    </div>
@endsection