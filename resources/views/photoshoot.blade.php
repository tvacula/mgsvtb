@extends('layout')

@section('title')
    @lang('web.mainMenu2_sub3')
@endsection

@section('content')
    <h2>@lang('web.mainMenu2_sub3')</h2>
    <p>Zde se můžete pokochat iluminacemi onoho slavného páru, vyvedené slovutným obrázkotvorcem Jankem Sedlářem, jehož další tvorbu můžete obdivovat na stránkách <a target="_blank" href="http://www.janeksedlar.com/">http://www.janeksedlar.com/</a>.</p>
    <p>Fotografie jsou pořízeny na skanzenu v Modré. Chtěli jsme zachytit jak naši moderní, tak středověkou stránku života. Na některých fotografiích se dokonce i prolínají. Nuže, kochejte se dle libosti.</p>
    <div class="gallery">
        <a target="_blank" href="{{ asset('images/gal10h.jpg') }}"><img class="img-fluid" src="{{ asset('images/gal10hsm.jpg') }}"></a>
        <a target="_blank" href="{{ asset('images/gal2h.jpg') }}"><img class="img-fluid" src="{{ asset('images/gal2hsm.jpg') }}"></a>
        <a target="_blank" href="{{ asset('images/gal3h.jpg') }}"><img class="img-fluid" src="{{ asset('images/gal3hsm.jpg') }}"></a>
        <a target="_blank" href="{{ asset('images/gal4h.jpg') }}"><img class="img-fluid" src="{{ asset('images/gal4hsm.jpg') }}"></a>
        <a target="_blank" href="{{ asset('images/gal5h.jpg') }}"><img class="img-fluid" src="{{ asset('images/gal5hsm.jpg') }}"></a>
        <a target="_blank" href="{{ asset('images/gal7h.jpg') }}"><img class="img-fluid" src="{{ asset('images/gal7hsm.jpg') }}"></a>
        <a target="_blank" href="{{ asset('images/gal8h.jpg') }}"><img class="img-fluid" src="{{ asset('images/gal8hsm.jpg') }}"></a>
        <a target="_blank" href="{{ asset('images/gal6w.jpg') }}"><img class="img-fluid" src="{{ asset('images/gal6wsm.jpg') }}"></a>
        <a target="_blank" href="{{ asset('images/gal9w.jpg') }}"><img class="img-fluid" src="{{ asset('images/gal9wsm.jpg') }}"></a>
        <a target="_blank" href="{{ asset('images/gal1w.jpg') }}"><img class="img-fluid" src="{{ asset('images/gal1wsm.jpg') }}"></a>
    </div>
@endsection