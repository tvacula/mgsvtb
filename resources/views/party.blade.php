@extends('layout')

@section('title')
    @lang('web.mainMenu3_sub3')
@endsection

@section('content')
    <h2>Hodování</h2>
    <p>Po skončení obřadu se přesuneme zpět do Retroparku Sejfy, kde vypukne konečně nějaké to nevázané veselí. To však neznamená, že bychom vzdali všechno plánování. Družičky zajísté připraví lecjaké veselé kratochvíle, proběhnou přípitky a proslovy a nemine nás ani krájení dortu.</p>
    <p>K protažení nohou na tanečním parketu a k potěše ucha nám zahrají slovutní bardi ze skupiny Pavouk Čichal Salám.</p>
    <p>K jídlu bude připravena šťavnatá kýta z pašíka a od šesté hodiny i velkolepá hostina, kam je každý z Vás zván, a to formou kdo si co na stolech uloví, to bude mít. To můžete spláchnout množstvím piva, vína, čí nápojů neopojných. Kdo po kořalce touží, má ji mít, ale do vlastní kapsy sáhnout musí.</p>
    <p>Oblečení už nemusí být nijak honosné, avšak dejte pozor, aby si Vás služebnictvo s nějakou holotou z podhradí nespletlo a nevyhodilo Vás jako trhany ven. Kdo bude mít na kalhotech díry, ten se se zlou potáže. Kalhoty slušně nad koleny zakrácené však nejsou ke škodě. Pro příznivce cizích slov, nyní se jedná o dress code smart casual.</p>
    <p>Pokud máte nějaké nápady, jak provést nějakou taškařici, sdělte je, prosíme, našim svědkům či družičkám a oni Vám dají vědět, zda se jedná o dobrý, chvályhodný nápad, který bude dozajista proveden, nebo jestli se jedná o takovou zhovadilost, že ji provedeme stejně. Je ale také možné, že Váš nápad nebude tak oceněn, a potom Vás prosíme, abyste si ho schovali na jindy. Rozhodně nestojíme o únosy nevěsty, zavírání do chomoutů a podobné veselosti, které měly již dávno zmizet v propadlišti dějin. Ač to někomu může přijít podivné, toto není sňatek politický, ale čistě z lásky a obě strany do něj vstupují více než dobrovolně.</p>

    <hr>
    <p>Program hodování:</p>
    <table class="program">
        <tr>
            <td>16:00 - 16:30</td><td class="filler" rowspan="6"></td><td>Příjezd zpět na Sejfy, pečení kýty</td>
        </tr>
        <tr>
            <td>16:30</td><td>Ubytovávání, čekání na novomanžele (kteří se mezitím fotí)</td>
        </tr>
        <tr>
            <td>18:00</td><td>Oficiální zahájení, proslov otce nevěsty, začátek rautu</td>
        </tr>
        <tr>
            <td>18:45</td><td>Proslov, házení kytice, krájení dortu, novomanželské hry</td>
        </tr>
        <tr>
            <td>20:00</td><td>První tanec novomanželů, tanec s rodiči, otevření tanečního parketu a volná</td>
        </tr>
        <tr>
            <td></td><td>Zábava</td>
        </tr>
    </table>
@endsection