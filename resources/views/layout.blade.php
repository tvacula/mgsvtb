<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <script src="https://code.jquery.com/jquery-3.3.1.min.js"
                integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
                crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
        <link rel="script" href="{{ asset('/js/app.js') }}">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet">
        @yield('head')
    </head>
    <body>
       <div class="row">
           <div class="flex-header d-none d-md-flex">
               <a class="title" href="{{ route('home') }}">@lang('web.title')</a>
           </div>
           @include ('navbar')
       </div>

        <div class="row">
            <div class="container content">
                @yield ('content')
            </div>
        </div>
    </body>
</html>
