<?php

return [
    'title' => 'MÍRA A GÁBINKA',
    'mainMenu1' => 'Úvod',
    'mainMenu2' => 'Záznamy kronikáře',
    'mainMenu2_sub1' => 'Bardův příběh',
    'mainMenu2_sub2' => 'Dlouhá cesta za prstenem',
    'mainMenu2_sub3' => 'Iluminace',
    'mainMenu3' => 'Program',
    'mainMenu3_sub1' => 'Před obřadem',
    'mainMenu3_sub2' => 'Obřad',
    'mainMenu3_sub3' => 'Hodování',
    'mainMenu4' => 'Ubytování',
    'mainMenu5' => 'Kontakty',
    'mainMenu5_sub1' => 'Kdo je kdo',
    'mainMenu5_sub2' => 'Mapy',
    'mainMenu5_sub3' => 'Další otázky',
    'mainMenu6' => 'Dary',
    'post_wedding' => 'Po svatbě',
    'pw_photos' => 'Iluminace ze svatby',
    'pw_contacts' => 'Dodavatelé, kontakty',
];